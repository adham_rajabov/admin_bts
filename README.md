admin generator
===============
this will generate admin

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist admin/yii2-admin "*"
```

or add

```
"admin/yii2-admin": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \adham\admin\AutoloadExample::widget(); ?>```