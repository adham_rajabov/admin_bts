<?php

/*

        $model = new \app\models\User;

        $fields = [
            // text
            ['type' => 'text', 'name' => 'name'],
            // textarea
            ['type' => 'textarea', 'name' => 'content', 'options' => ['rows' => 5]],
            // password
            ['type' => 'password', 'name' => 'content', 'options' => ['maxlength' => 255]],
            // dropdown
            ['type' => 'dropdown', 'name' => 'content', 'items' => \yii\helpers\ArrayHelper::map(\app\models\Tuman::find()->all(), 'id', 'name')],
            // checkbox
            ['type' => 'checkbox', 'name' => 'content'],
            // hidden
            ['type' => 'hidden', 'name' => 'id'],
            // dropdonwMy
            ['type' => 'dropdownMy', 'name' => 'tip', 'items' => ['fuqaro' => 'Фукаро', 'tashkilot' => 'Ташкилот'], 'options' => ['name' => 'Murojatchi[tip][]']],

            ['type' => 'dropdown', 'name' => 'viloyatId', 'items' => \yii\helpers\ArrayHelper::map(\app\models\Viloyat::find()->all(), 'id', 'name')],

            // related table
            [   'type' => 'manyToMany', 
                'name' => 'userId', // the name for getting through POST

                'related' => [ // if the model related to other field
                                'relatedId' => 'user-viloyatid',            // related id
                                'action' => url(['site/change-viloyat']),   // related action
                                'autoload' => true,                         // do autoload when first loading
                            ],

                'param' => ['manyToMany'=>'\app\models\UserTuman', // many to many model name
                                'firstId'=>'userId', // related id to current model
                                'secondId'=>'tumanId', // second id for second table
                                'items' => \yii\helpers\ArrayHelper::map(\app\models\Tuman::find()->all(), 'id', 'name') // list of items
                            ],

                'title' => 'Туман', // title name when showing in form
                'options' => ['multiple' => true, 'id' => 'tumanlar'] // options
            ]            


            // one to many table
            // murojatchi
            [   'type' => 'oneToMany', 
                'name' => 'Murojatchi', // the name for getting through POST (it must be identical with oneToMany table)

                'param' => ['oneToMany'=>'\app\models\Murojatchi', // one to many model name
                                'relatedId'=>'murojatId', // related id to current model
                                'fields' => [

                                    // tip
                                    ['type' => 'dropdownMy', 'name' => 'tip', 'items' => ['fuqaro' => 'Фукаро', 'tashkilot' => 'Ташкилот'], 'options' => ['name' => 'Murojatchi[tip][]']],

                                    // name
                                    ['type' => 'text', 'name' => 'name', 'options' => ['name' => 'Murojatchi[name][]']],

                                    // address
                                    ['type' => 'textarea', 'name' => 'address', 'options' => ['rows' => 5, 'name' => 'Murojatchi[address][]']],

                                ]
                            ],

                'title' => 'Мурожаатчи', // title name when showing in form
                'options' => ['id' => 'murojatchi'] // options [id required to differ oneToMany's]
            ],
        ];

        if (isset($_POST['Settings'])) {

            $a = \adham\admin\AutoloadExample::widget([
                'post' => $_POST,
                'legend' => $legend,
                'model' => $model,
                'fields' => $fields,
            ]);

            echo $a;


            // $model = $a;
            // return false;
            // $this->redirect(['site/index']);

        } 

        // echo '<br/><br/><br/><br/><br/>';pr($model->errors);

        $options = [
            'legend' => 'setting ni uzgartirish'
        ];

        return $this->render('//update', [
            'options' => $options,
            'model' => $model,
            'fields' => $fields,            
        ]);



    public function actionChangeViloyat()
    {
        $model = new User;

        $relatedId = $_POST['relatedId'];

        $fields = [

            [   'type' => 'manyToMany', 
                'name' => 'userId', // the name for getting through POST

                'related' => [ // if the model related to other field
                                'relatedId' => 'user-viloyatid',            // related id
                                'action' => url(['site/change-viloyat']),   // related action
                                'autoload' => true,                         // do autoload when first loading
                            ],

                'param' => ['manyToMany'=>'\app\models\UserTuman', // many to many model name
                                'firstId'=>'userId', // related id to current model
                                'secondId'=>'tumanId', // second id for second table
                                'items' => \yii\helpers\ArrayHelper::map(\app\models\Tuman::find()->where('viloyatId='.$relatedId)->all(), 'id', 'name') // list of items
                            ],

                'title' => 'Туман', // title name when showing in form
                'options' => ['multiple' => true, 'id' => 'tumanlar'] // options
            ]
        ];

        echo $this->renderPartial('//update', [
            'options' => ['submitButton' => false],
            'model' => $model,
            'fields' => $fields,            
        ]);
    }        

text 			- http://www.yiiframework.com/doc-2.0/yii-widgets-activefield.html#textInput()-detail
textarea 		- http://www.yiiframework.com/doc-2.0/yii-widgets-activefield.html#textarea()-detail
dropdown 		- http://www.yiiframework.com/doc-2.0/yii-widgets-activefield.html#dropDownList()-detail
password 		- http://www.yiiframework.com/doc-2.0/yii-widgets-activefield.html#passwordInput()-detail
checkbox 		- http://www.yiiframework.com/doc-2.0/yii-widgets-activefield.html#checkbox()-detail

*/
namespace adham\admin;

class AutoloadExample extends \yii\base\Widget
{

	/**
	 * model name
	 * @var string
	 */
	public $model;

	/**
	 * posted data
	 * @var array // posted data
	 */
	public $post = array();

	/** 
	 * fields to show
	 *	- model string // model name (if not given it gets default model)
	 *	- type string // type of fields [text, textarea, date, wysiwyg, checkbox]
	 *	- name string // field name
	 *	- params string or array() // params
	 *	- htmlOptions array() // html options
	 * @var array holds an array of fields
	 */
	public $fields = array();

	/**
	 * optional parameters
	 * @var array // 
	 *		'legend' => 'this is test legend'			// legend for form
	 *		'submitButton' => true,						// showing or not showing submit button
	 *		'backUrl' => 'site/mainCategory',			// back url
	 */
	public $options = array();

	/**
	 * Legend for form
	 */
	public $submitButton = true;

    public function run()
    {    	
		if (count($this->post)>0) {

			return $this->saveData();

		} else {

			return $this->updateData();
		}
    }

	/**
	 * update Model
	 */
	public function updateData()
	{
		$params = [
			'params' => [
					'model' => $this->model,
					'fields' => $this->fields,
					'options' => $this->options,
				]
			];

		if ($this->model->isNewRecord) {

			return $this->render('@vendor/admin/yii2-admin/views/create', $params);

		} else {

			return $this->render('@vendor/admin/yii2-admin/views/update', $params);

		}


	}

	/**
	 * save Model
	 */
	public function saveData()
	{

		// // get model name
		$modelName = get_class($this->model);

		// // get id name of a main model
		$mainIdName = $this->model->tableSchema->primaryKey;
		$mainIdName = $mainIdName[0];

		// // get id
		$id = ($this->model->$mainIdName>0)?$this->model->$mainIdName:0;

        // we need get old file name before save
        $oldFile = '';
        if ($id>0) {
            
            if (count($this->fields)>0) {

                foreach($this->fields as $field) {

                    // file upload
                    if ($field['type']=='file') {

                        $oldFile = $this->model->$field['name'];
                    }
                }
            }
        }

        if ($this->model->load($this->post) && $this->model->save()) {

        	if ($id==0) {

	        	$id = \Yii::$app->db->getLastInsertId();

        	}

			if (count($this->fields)>0) {

				foreach($this->fields as $field) {

                    // check if multiSelect field exists
					if ($field['type']=='manyToMany') {
						
						// delete old values
						$field['param']['manyToMany']::deleteAll($field['param']['firstId'].'='.$id);

						if (isset($this->post[$field['name']])) {

							$middleValues = $this->post[$field['name']];

							foreach ($middleValues as $middleValue) {

								$middleModel = new $field['param']['manyToMany'];
								$middleModel->$field['param']['firstId'] = $id;								
								$middleModel->$field['param']['secondId'] = $middleValue;
								$middleModel->save();
							}
						}
					}

                    // check if oneToMany field exists
                    if ($field['type']=='oneToMany') {
                        
                        // delete old values
                        $field['param']['oneToMany']::deleteAll($field['param']['relatedId'].'='.$id);

                        $oneToMany = new $field['param']['oneToMany'];
                        $modelObjectName = ucfirst($oneToMany->tableSchema->name);

                        if (isset($this->post[$modelObjectName])) {

                            $oneToManyValues = $this->post[$modelObjectName];

                            $firstElem = reset($oneToManyValues);

                            foreach ($firstElem as $key => $oneToManyValue) {

                                // skip for hidden values (for block routine)
                                if ($key==0) continue;

                                $oneToManyModel = new $oneToMany;

                                $oneToManyModel->$field['param']['relatedId'] = $id;

                                foreach ($field['param']['fields'] as $oneToManyField) {

                                    $oneToManyModel->$oneToManyField['name'] = $oneToManyValues[$oneToManyField['name']][$key];

                                }

                                $oneToManyModel->save();                                

                            }

                        }
                    }

                    // file upload
                    if ($field['type']=='file') {

                        $uploadPath = $field['options']['uploadPath'];
                        $file = \yii\web\UploadedFile::getInstance($this->model, $field['name']);
                        $delFile = $uploadPath.$oldFile;
                        // pr($oldFile);die;

                        if (!empty($file->name)) {

                                $myfile = pathinfo($file->name);

                                $file->saveAs($uploadPath.time()."_".$id.'.'.$myfile['extension']);
                                $this->model->$field['name'] = time()."_".$id.'.'.$myfile['extension'];
                                $this->model->save();

                                @unlink($delFile);

                        } else {

                            $this->model->$field['name'] = $oldFile;
                            $this->model->save();

                        }

                    }

				}
			}

			$result['status'] = true;
			$result['model'] = $this->model;

        	// return $result;
        	return serialize($result);

        } else {

			$result['status'] = false;
			$result['model'] = $this->model;

        	return serialize($result);
        }

	}

}
