<?php
/**
 * Created by PhpStorm.
 * User: nabijon
 * Date: 5/24/15
 * Time: 4:41 PM
 */
use kartik\money\MaskMoney;

// Money mask widget with ActiveForm and model validation rule (amounts between 1 to 100000).
// Initial value is set to 1400.50.
echo $form->field($model, $field['name'])->widget(MaskMoney::classname(), [
    'pluginOptions' => [
        'prefix' => isset($field['options']['prefix']) ? $field['options']['prefix'] : '',
        'suffix' => isset($field['options']['suffix']) ? $field['options']['suffix'] : '',
        'allowNegative' => isset($field['options']['allowNegative']) ? $field['options']['allowNegative'] : false,
        'decimal' => '',
        //'precision' => 0,
    ]
]);

//// SETTINGS in Yii::$app->params
//'maskMoneyOptions' => [
//    'prefix' => html_entity_decode('&#8377; '), // the Indian Rupee Symbol
//    'suffix' => '',
//    'affixesStay' => true,
//    'thousands' => ',',
//    'decimal' => '.',
//    'precision' => 2,
//    'allowZero' => true,
//    'allowNegative' => true,
//]
//
//// A disabled MaskMoney input.
//echo MaskMoney::widget([
//    'name' => 'amount_1',
//    'value' => 28239.35,
//    'disabled' => true
//]);