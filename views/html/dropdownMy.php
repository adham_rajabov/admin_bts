<div class="form-group field-<?=getYiiName($field['name'])?> required">
    <?php
    if (isset($field['title'])) {
        echo '<label class="control-label">'.$field['title'].'</label>&nbsp;';
    }

    echo \yii\helpers\Html::dropDownList(
        $field['name'],
        isset($field['selection'])?$field['selection']:null,
        $field['items'],
        isset($field['options'])?$field['options']:[]
    );

    ?>
</div>

