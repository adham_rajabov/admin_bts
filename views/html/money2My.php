<?php
/**
 * Created by PhpStorm.
 * User: nabijon
 * Date: 5/24/15
 * Time: 4:44 PM
 */
use kartik\money\MaskMoney;?>

<div class="form-group field-<?=getYiiName($field['name'])?> required">
<?php
if (isset($field['title'])) {
    echo '<label class="control-label">'.$field['title'].'</label>&nbsp;';
}

// Money mask widget without ActiveForm or model and settings defaulted from
// `maskMoneyOptions` in `Yii::$app->params`
echo MaskMoney::widget([
    'name' => $field['name'],
    'value' => $field['value']
]).'<br>';

//// SETTINGS in Yii::$app->params
//'maskMoneyOptions' => [
//    'prefix' => html_entity_decode('&#8377; '), // the Indian Rupee Symbol
//    'suffix' => '',
//    'affixesStay' => true,
//    'thousands' => ',',
//    'decimal' => '.',
//    'precision' => 2,
//    'allowZero' => true,
//    'allowNegative' => true,
//]
//
//// A disabled MaskMoney input.
//echo MaskMoney::widget([
//    'name' => 'amount_1',
//    'value' => 28239.35,
//    'disabled' => true
//]);