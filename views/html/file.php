    <?= $form->field($model, $field['name'])->widget(\kartik\widgets\FileInput::classname(), [

	    	'options' => isset($field['options']['options'])?$field['options']['options']:[],

	        'pluginOptions' => isset($field['options']['pluginOptions'])?$field['options']['pluginOptions']:[],
        ]);
    ?>
