<?php

echo \kartik\widgets\DatePicker::widget([
    'model' => $model,
    'form' => $form,
    'attribute' => $field['name'],
    'options' => isset($field['options'])?$field['options']:[],
    'pluginOptions' => isset($field['pluginOptions'])?$field['pluginOptions']:[],
]);

?>