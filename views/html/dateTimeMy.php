<div class="form-group field-<?=getYiiName($field['name'])?> required">
    <?php
    if (isset($field['title'])) {
        echo '<label class="control-label">'.$field['title'].'</label><br>';
    }

    echo \kartik\widgets\DateTimePicker::widget([
        'name' => $field['name'],
        'value' => isset($field['value'])?$field['value']:'',
        'options' => array_merge(['placeholder1' => 'Select operating time ...', 'style'=>'background:none; color:#CBBAAF;', 'id'=>'event-datetime'], isset($field['options'])?$field['options']:[]),
        'removeButton' => false,
        'pickerButton' => ['icon' => 'time'],
        'pluginOptions' => array_merge([
            'todayHighlight' => true,
            'autoclose' => true
        ], isset($field['pluginOptions'])?$field['pluginOptions']:[])
    ]);

    ?>
</div>