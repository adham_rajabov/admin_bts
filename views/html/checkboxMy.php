<div class="form-group field-<?=getYiiName($field['name'])?> required">
<?php
if (isset($field['title'])) {
    echo '<label class="control-label">'.$field['title'].'</label>&nbsp;';
}

echo \kartik\helpers\Html::activeCheckbox($field['name'], isset($field['options'])?$field['options']:[]) ?>