<?php
/**
 * Created by PhpStorm.
 * User: nabijon
 * Date: 5/24/15
 * Time: 4:41 PM
 */

echo $form->field($model, $field['name'])->widget(\extead\autonumeric\AutoNumeric::classname(), [
    'pluginOptions' => [
        'aSep' => isset($field['options']['pluginOptions']['thousandsSeparator']) ? $field['options']['pluginOptions']['thousandsSeparator'] : ' ', // thousands separator
        'mDec' => isset($field['options']['pluginOptions']['numberPrecision']) ? $field['options']['pluginOptions']['numberPrecision'] : 2, // number precision
        'aDec' => isset($field['options']['pluginOptions']['decimalSeparator']) ? $field['options']['pluginOptions']['decimalSeparator'] : '.', // decimal separator
        'aSign' => isset($field['options']['pluginOptions']['currencySymbol']) ? $field['options']['pluginOptions']['currencySymbol'] : '', // currency symbol,
        'pSign' => 'p', // is currency symbol prefix or postfix
        'vMin' => '-9999999999.99', // minimum value
        'vMax' => '9999999999.99', // maximum value
    ]
]);