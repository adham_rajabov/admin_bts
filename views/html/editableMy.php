<?php
use kartik\editable\Editable;
?>

<div class="form-group field-<?=getYiiName($field['name'])?> required">
    <?php
    if (0&&isset($field['title'])) {
        echo '<label class="control-label">'.$field['title'].'</label>&nbsp;';
    }

    $defaultPluginsOptions = [
        'prefix' => '',
        'suffix' => '',
        'affixesStay' => true,
        'thousands' => ' ',
        'decimal' => '.',
        'precision' => 0,
        'allowZero' => true,
        'allowNegative' => false,
        'autoclose' => true,
        'todayHighlight' => true,
        'allowClear'=> false,
        'format' => 'dd-MM-yyyy HH:ii P',
    ];

    $defaultOptions = [
        'class'=>'form-control',
        'options' => ['placeholder'=>'----', 'style' => 'width:300px;'],
        'convertFormat' => true,
        'pluginOptions' => array_merge(
            $defaultPluginsOptions,
            isset($field['pluginOptions']) ? $field['pluginOptions'] : []
        ),
    ];

    $inputType = isset($field['inputType']) ? $field['inputType'] : 'INPUT_TEXT';
    if(is_string($inputType)) {
        $ref = new ReflectionClass('kartik\editable\Editable');
        $inputType = $ref->getConstant($inputType);
    }

// A simple editable content used without a model and the value defaulting to display value.
// Note that by default the input type is set to `Editable::INPUT_TEXT` and the editable format
// is displayed as a `Editable::FORMAT_LINK`.
echo Editable::widget([
    'name'=>$field['name'],
    'asPopover' => isset($field['popup']) ? $field['popup'] : true,
    'value' => $field['value'],
    'displayValue' => $field['value'],
    'header' => isset($field['title']) ? $field['title'] : ' ',
    //'showButtons' => isset($field['showButtons']) ? $field['showButtons'] : false,
    //'buttonsTemplate' => isset($field['buttonsTemplate']) ? $field['buttonsTemplate'] : false,
    //'submitOnEnter' => true,
    'size'=>'sm',
    'inputType' => $inputType,
    'options' => isset($field['options']) ? array_merge($field['options'], $defaultOptions) : $defaultOptions,
    'placement' => 'bottom',
    'formOptions' => isset($field['formOptions']) ? $field['formOptions'] : [],
    'pluginOptions' => array_merge(
        isset($field['pluginOptions']) ? $field['pluginOptions'] : [],
        $defaultPluginsOptions
    ),
    'pluginEvents' => isset($field['pluginEvents']) ? $field['pluginEvents'] : [],
    'contentOptions' => isset($field['contentOptions']) ? $field['contentOptions'] : []
]);?>

</div>

<?php

$fieldName = $field['name'];

ob_start(); ?>

<script>

    $(function(){

        $('body').on('keypress', $('.kv-editable-form'), function(e) {

            var keycode = (event.keyCode ? event.keyCode : event.which);

            if(keycode == 13) {
                $(e.target).closest('.kv-editable-form').submit();
            }
        });

        $('body').on('click', '.kv-editable-value', function(){

            if(<?=((isset($field['showButtons']) && !$field['showButtons']))?1:0?>) {
                $('.kv-editable-submit').attr('style', 'display: none');
                $('.kv-editable-reset').attr('style', 'display: none');
            }

            var thiz = $(this);
            var input = thiz.closest(".kv-editable-value");

//            input.on("change", function () {
//                var val = $(this).siblings("input").val();
//                val = Number(val.replace(/[^0-9\.]+/g,""));
//                $(this).val(val);
//            });
//
//            setTimeout(function(){
//                var input = thiz.closest('[name="<?//=$fieldName;?>//"]');
//                input.siblings("input").val(input.val());
//                input.siblings("input").maskMoney("mask");
//            },1);

            var input =  $(this).closest('td').find('textarea')[0];
            if(isUndefined(input)) input = $(this).closest('td').find('input[type="text"]')[0];
//info(input);
            setTimeout(function(){
                input.focus();
                $(input).closest('.select2-container').select2('open');
            }, 300);

        });

    });

</script>

<?php $js = ob_get_clean();

$this->registerJs(removeScriptTag($js));

?>