<?php

// Usage without a model
echo '<label class="control-label">Загрузить файл</label>';

echo kartik\widgets\FileInput::widget([
    'name' => !empty($field['name']) ? $field['name'] : 'attachment_3',
    'options' => isset($field['options']['options'])?$field['options']['options']:[],
    'pluginOptions' => isset($field['options']['pluginOptions'])?$field['options']['pluginOptions']:[],
]);

// $form->field($model, $field['name'])->widget(\kartik\widgets\FileInput::classname(), [
//
//    	'options' => isset($field['options']['options'])?$field['options']['options']:[],
//
//        'pluginOptions' => isset($field['options']['pluginOptions'])?$field['options']['pluginOptions']:[],
//    ]);
?>

