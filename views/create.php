<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\CheckTypeSub $model
 */

if (isset($params['options']['legend'])) {
	$this->title = $params['options']['legend'];
}

if (isset($params['options']['breadcrumbs'])) {

	$this->params['breadcrumbs'] = $params['options']['breadcrumbs'];

}

?>
<br/>
<div class="check-type-sub-update">

	<?php if (isset($params['options']['legend'])) : ?>
    	<h1><?= Html::encode($params['options']['legend']) ?></h1>
    <?php endif; ?>

    <?= $this->render('@vendor/admin/yii2-admin/views/_form', $params) ?>

</div>
